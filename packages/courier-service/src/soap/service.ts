export default {
    MyService: {
        MyPort: {
            MyFunction: args => ({
                name: args.name
            }),

            // This is how to define an asynchronous function with a callback.
            MyAsyncFunction: (args, callback) => {
                // do some work
                callback({
                    name: args.name
                });
            },

            // This is how to define an asynchronous function with a Promise.
            MyPromiseFunction: args => new Promise((resolve) => {
                // do some work
                resolve({
                    name: args.name
                });
            }),

            // This is how to receive incoming headers
            HeadersAwareFunction: (args, cb, headers) => ({
                name: headers.Token
            }),

            // You can also inspect the original `req`
            reallyDetailedFunction: (args, cb, headers, req) => {
                console.log('SOAP `reallyDetailedFunction` request from ' + req.connection.remoteAddress);
                return {
                    name: headers.Token
                };
            }
        }
    }
};
