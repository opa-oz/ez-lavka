import http from 'http';
import chalk from 'chalk'
import * as soap from 'soap'
import fs from 'fs';
import path from 'path';

import app from './src/app';
import soapService from './src/soap/service';

const PATH_TO_SOAP = path.join(__dirname, '/resources/warehouse.wsdl');

const { COURIER_PORT, SOAP } = process.env;
const port = COURIER_PORT ? parseInt(COURIER_PORT, 10) : 4031;

const httpServer = http.createServer(app);

httpServer.on('listening', () => {
    console.log(`${chalk.bold.green('[CourierService]')} Server is listening at port ${chalk.bold(port)}`);
})

httpServer.listen(port);
if (SOAP) {
    // https://www.npmjs.com/package/soap
    const xml = fs.readFileSync(PATH_TO_SOAP, 'utf8')

    soap.listen(httpServer, '/wsdl', soapService, xml, () => {
        console.log(`${chalk.bold.green('[CourierService]')} Soap is listening at ${chalk.bold('/wsdl')}`);
    })
}
