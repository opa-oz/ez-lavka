import express from 'express';
import { config } from 'dotenv';
import bodyParser from 'body-parser'
import bodyParserXml from 'body-parser-xml';

config();
bodyParserXml(bodyParser);

const app = express();

const { JSON, XML } = process.env;

if (JSON) {
    app.use(bodyParser.json())
}

if (XML) {
    app.use(bodyParser.xml())
}

export default app;
