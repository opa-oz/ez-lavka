import http from 'http';
import chalk from 'chalk'
import * as soap from 'soap'
import fs from 'fs';
import path from 'path';

import app from './src/app';
import soapService from './src/soap/service';

const PATH_TO_SOAP = path.join(__dirname, '/resources/warehouse.wsdl');

const { WAREHOUSE_PORT, SOAP } = process.env;
const port = WAREHOUSE_PORT ? parseInt(WAREHOUSE_PORT, 10) : 4001;

const httpServer = http.createServer(app);

httpServer.on('listening', () => {
    console.log(`${chalk.bold.blue('[Warehouse]')} Server is listening at port ${chalk.bold(port)}`);
})

httpServer.listen(port);
if (SOAP) {
    // https://www.npmjs.com/package/soap
    const xml = fs.readFileSync(PATH_TO_SOAP, 'utf8')

    soap.listen(httpServer, '/wsdl', soapService, xml, () => {
        console.log(`${chalk.bold.blue('[Warehouse]')} Soap is listening at ${chalk.bold('/wsdl')}`);
    })
}
