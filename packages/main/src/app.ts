import express from 'express';
import { config } from 'dotenv';
import bodyParser from 'body-parser'
import bodyParserXml from 'body-parser-xml';

config();
bodyParserXml(bodyParser);

const app = express();

app.use(bodyParser.json())
app.use(bodyParser.xml())

export default app;
