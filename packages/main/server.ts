import http from 'http';
import chalk from 'chalk'
import app from './src/app'

const { MAIN_PORT } = process.env;
const port = MAIN_PORT ? parseInt(MAIN_PORT, 10) : 4000;

const httpServer = http.createServer(app);

httpServer.on('listening', () => {
    console.log(`${chalk.bold.red('[Main]')} Server is listening at port ${chalk.bold(port)}`);
})

httpServer.listen(port);
