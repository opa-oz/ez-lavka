# ez-lavka

[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lerna.js.org/)[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lerna.js.org/)
---

## Getting started
First of all, switch your NodeJS version:
```bash
$ nvm install v10 && nvm use
```

Next, install Lerna:
```bash
$ yarn global add lerna
OR
$ npm i -g lerna
```

Next, bootstrap all dependencies
```bash
$ lerna bootstrap
```

Finally, you can run each package:
```bash
$ cd packages/main
$ yarn dev
OR 
$ npm run dev
```
---
## Services ports:
- **Main** - `4000`
- **Warehouse #1** - `4021`
- **Warehouse #2** - `4022`
- **Warehouse #3** - `4023`
- **Warehouse #4** - `4024`
- **Courier Service #1** - `4031`
- **Courier Service #2** - `4032`
- **Courier Service #3** - `4033`
- **Courier Service #4** - `4034`
